<?php


namespace Trial;


use Trial\Form\ProductForm;
use Trial\Repository\ProductRepository;
use Trial\Service\Controller;
use Trial\Service\Http\Request;


class ProductController extends Controller
{
    public function process(Request $request)
    {
        switch ($request->getQueryString()) {
            case 'product/new':
                return $this->createProduct($request);
            case 'product/list':
                return $this->productList();
            case 'product/delete':
                return $this->deleteProduct($request);
        }
    }

    private function productList()
    {
        $repository = new ProductRepository();

        $this->render('product-list.php', [
            'products' => $repository->getAllProductsWithAttributes()
        ]);

    }

    private function createProduct(Request $request)
    {
        $repository = new ProductRepository();

        if ($request->getMethod() === 'POST') {
            $repository->saveProduct((new ProductForm())->handle($request));
        }

        $this->render('new-product.php', [
            'productTypes' => $repository->getAllProductTypes()
        ]);
    }

    private function deleteProduct(Request $request)
    {
        if ($request->getPostArrayParam('product') !== null) {
            (new ProductRepository())->deleteProduct($request->getPostArrayParam('product'));
        }
        header("Location: http://localhost/trial/product/list");
    }
}