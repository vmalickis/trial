<?php


namespace Trial\Repository;


use Trial\Product\AbstractProduct;
use Trial\Product\Factory\BookFactory;
use Trial\Product\Factory\DiscStorageFactory;
use Trial\Product\Factory\FurnitureFactory;
use Trial\Product\Products\Book;
use Trial\Product\Products\DiscStorage;
use Trial\Product\Products\Furniture;
use Trial\Product\ProductType;
use Trial\Product\ProductTypes;
use Trial\Service\DBConnector;


class ProductRepository extends DBConnector
{
    /**
     * @return ProductType[]
     */
    public function getAllProductTypes(): array
    {
        $query = $this->connection->query("SELECT * FROM `product_type`");

        $types = [];
        $result = $query->fetch_all(MYSQLI_ASSOC);

        foreach ($result as $item) {
            $types[] = (new ProductType())
                        ->setId($item['id'])
                        ->setType($item['type'])
            ;
        }
        return $types;
    }

    /**
     * @return DiscStorage|Book|Furniture []
     */
    public function getAllProductsWithAttributes()
    {
        $query = $this->connection->query("
            SELECT p.id, p.sku, p.name, p.price, p.type_id,
                   s.size,
                   w.weight,
                   d.height, d.width, d.length
            FROM `product` p
              LEFT JOIN `product_size` s ON p.id = s.product_id
              LEFT JOIN `product_weight` w ON p.id = w.product_id
              LEFT JOIN `product_dimensions` d ON p.id = d.product_id
            ORDER BY p.type_id
        ");

        $result = $query->fetch_all(MYSQLI_ASSOC);
        $products = [];

        foreach ($result as $product) {
            switch ($product['type_id']) {
                case ProductTypes::TYPE_WITH_SIZE:
                    $products[] = (new DiscStorageFactory())
                                    ->produce($product['sku'],
                                              $product['name'],
                                              $product['price'],
                                              $product['type_id'],
                                              $product['size'],
                                              $product['id'])
                    ;
                    break;
                case ProductTypes::TYPE_WITH_WEIGHT:
                    $products[] = (new BookFactory())
                                    ->produce($product['sku'],
                                              $product['name'],
                                              $product['price'],
                                              $product['type_id'],
                                              $product['weight'],
                                              $product['id'])
                    ;
                    break;
                case ProductTypes::TYPE_WITH_DIMENSIONS:
                    $products[] = (new FurnitureFactory())
                                    ->produce($product['sku'],
                                              $product['name'],
                                              $product['price'],
                                              $product['type_id'],
                                              $product['height'],
                                              $product['width'],
                                              $product['length'],
                                              $product['id'])
                    ;
                    break;
            }
        }
        return $products;
    }

    /**
     * @param AbstractProduct $product
     */
    public function saveProduct(AbstractProduct $product)
    {
        $this->connection->query("
            INSERT INTO `product` (sku, name, price, type_id)
                VALUES 
                (UPPER('{$product->getSku()}'),
                '{$product->getName()}',
                '{$product->getPrice()}',
                '{$product->getType()}')
        ");
        $lastInsertId = $this->connection->insert_id;

        if ($lastInsertId) {
            switch ($product->getType()) {
                case ProductTypes::TYPE_WITH_SIZE:
                    $this->connection->query("
                        INSERT INTO `product_size` (product_id, size)
                            VALUES ('{$lastInsertId}', '{$product->getSize()}')
                    ");
                break;
                case ProductTypes::TYPE_WITH_WEIGHT:
                    $this->connection->query("
                        INSERT INTO `product_weight` (product_id, weight)
                            VALUES ('{$lastInsertId}', '{$product->getWeight()}')
                    ");
                    break;
                case ProductTypes::TYPE_WITH_DIMENSIONS:
                    $this->connection->query("
                        INSERT INTO `product_dimensions` (product_id, height, width, length)
                            VALUES (
                            '{$lastInsertId}',
                            '{$product->getHeight()}',
                            '{$product->getWidth()}',
                            '{$product->getLength()}'
                            )
                    ");
                    break;
            }
        } else {
            throw new \mysqli_sql_exception('Product not saved, try again');
        }
    }

    /**
     * @param $productIds int[]
     */
    public function deleteProduct($productIds)
    {
        foreach ($productIds as $id) {
            $this->connection->query("DELETE FROM `product` WHERE `id` = '{$id}'");
        }
    }
}