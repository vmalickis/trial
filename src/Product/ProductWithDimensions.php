<?php


namespace Trial\Product;


interface ProductWithDimensions
{
    /** @return string */
    public function getHeight(): string;

    /** @return string */
    public function getWidth(): string;

    /** @return string */
    public function getLength(): string;
}