<?php
namespace Trial\Product\Factory;

use Trial\Product\Products\Book;

class BookFactory
{
    /**
     * @param int $id
     * @param string $sku
     * @param string $name
     * @param string $price
     * @param int $typeId
     * @param string $weight
     * @return Book
     */
    public function produce($sku, $name, $price, $typeId, $weight, $id = null)
    {
        $discStorage = new Book();
        $discStorage->setSku($sku);
        $discStorage->setName($name);
        $discStorage->setPrice($price);
        $discStorage->setType($typeId);
        $discStorage->setWeight($weight);
        $discStorage->setId($id);
        
        return $discStorage;
    }
}

