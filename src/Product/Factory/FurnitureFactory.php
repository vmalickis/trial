<?php
namespace Trial\Product\Factory;

use Trial\Product\Products\Furniture;

class FurnitureFactory
{
    /**
     * @param int $id
     * @param string $sku
     * @param string $name
     * @param string $price
     * @param int $typeId
     * @param string $height
     * @param string $width
     * @param string $length
     * @return Furniture
     */
    public function produce($sku, $name, $price, $typeId, $height, $width, $length, $id = null)
    {
        $furniture = new Furniture();
        $furniture->setSku($sku);
        $furniture->setName($name);
        $furniture->setPrice($price);
        $furniture->setType($typeId);
        $furniture->setHeight($height);
        $furniture->setWidth($width);
        $furniture->setLength($length);
        $furniture->setId($id);
        
        return $furniture;
    }
}

