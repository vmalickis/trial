<?php
namespace Trial\Product\Factory;

use Trial\Product\Products\DiscStorage;

class DiscStorageFactory
{
    /**
     * @param int $id
     * @param string $sku
     * @param string $name
     * @param string $price
     * @param int $typeId
     * @param string $size
     * @return DiscStorage
     */
    public function produce($sku, $name, $price, $typeId, $size, $id = null)
    {
        $discStorage = new DiscStorage();
        $discStorage->setSku($sku);
        $discStorage->setName($name);
        $discStorage->setPrice($price);
        $discStorage->setType($typeId);
        $discStorage->setSize($size);
        $discStorage->setId($id);
        
        return $discStorage;
    }
}

