<?php


namespace Trial\Product;


interface ProductWithSize
{
    const SIZE_MEASUREMENT_UNIT = 'MB';

    /** @return string */
    public function getSize(): string;
}