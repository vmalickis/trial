<?php


namespace Trial\Product;


class ProductTypes
{
    const TYPE_WITH_SIZE = 1;
    const TYPE_WITH_WEIGHT = 2;
    const TYPE_WITH_DIMENSIONS = 3;
}