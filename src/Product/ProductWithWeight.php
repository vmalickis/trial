<?php


namespace Trial\Product;


interface ProductWithWeight
{
    const WEIGHT_MEASUREMENT_UNIT = 'KG';

    /** @return string */
    public function getWeight(): string;
}