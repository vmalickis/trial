<?php


namespace Trial\Product\Products;

use Trial\Product\AbstractProduct;
use Trial\Product\ProductWithDimensions;


class Furniture extends AbstractProduct implements ProductWithDimensions
{
    const DIMENSION_DELIMITER = 'x';

    /** @var string */
    private $height;

    /** @var string */
    private $width;

    /** @var string */
    private $length;

    /**
     * @return string
     */
    public function getViewLayerDimensions()
    {
        return $this->height
            . self::DIMENSION_DELIMITER
            . $this->width
            . self::DIMENSION_DELIMITER
            . $this->length
        ;
    }

    /**
     * @return string
     */
    public function getHeight(): string
    {
        return $this->height;
    }

    /**
     * @param string $height
     * @return Furniture
     */
    public function setHeight(string $height): Furniture
    {
        $this->height = $height;
        return $this;
    }

    /**
     * @return string
     */
    public function getWidth(): string
    {
        return $this->width;
    }

    /**
     * @param string $width
     * @return Furniture
     */
    public function setWidth(string $width): Furniture
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @return string
     */
    public function getLength(): string
    {
        return $this->length;
    }

    /**
     * @param string $length
     * @return Furniture
     */
    public function setLength(string $length): Furniture
    {
        $this->length = $length;
        return $this;
    }
}