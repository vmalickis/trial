<?php


namespace Trial\Product\Products;


use Trial\Product\AbstractProduct;
use Trial\Product\ProductWithSize;

class DiscStorage extends AbstractProduct implements ProductWithSize
{
    /** @var string */
    private $size;

    /**
     * @return string
     */
    public function getSize(): string
    {
        return $this->size;
    }

    /**
     * @param string $size
     * @return DiscStorage
     */
    public function setSize(string $size): DiscStorage
    {
        $this->size = $size;
        return $this;
    }
}