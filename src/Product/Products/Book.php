<?php


namespace Trial\Product\Products;


use Trial\Product\AbstractProduct;
use Trial\Product\ProductWithWeight;

class Book extends AbstractProduct implements ProductWithWeight
{
    /** @var string */
    private $weight;

    /**
     * @return string
     */
    public function getWeight(): string
    {
        return $this->weight;
    }

    /**
     * @param string $weight
     * @return Book
     */
    public function setWeight(string $weight): Book
    {
        $this->weight = $weight;
        return $this;
    }
}