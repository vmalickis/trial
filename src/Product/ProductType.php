<?php


namespace Trial\Product;


class ProductType
{
    /** @var int */
    private $id;

    /** @var string */
    private $type;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ProductType
     */
    public function setId(int $id): ProductType
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return ProductType
     */
    public function setType(string $type): ProductType
    {
        $this->type = $type;
        return $this;
    }
}