<?php


namespace Trial\Service\Http;


class Request
{
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';

    /** @var string */
    private $method;

    /** @var string */
    private $queryString;

    /** @var array */
    private $queryStringParts;

    /** @var array */
    private $postParams;

    /**
     * Request constructor.
     * @param string $method
     * @param string $queryString
     * @param array $postParams
     */
    public function __construct(string $method, string $queryString, array $postParams)
    {
        $this->method = $method;
        $this->queryString = $queryString;
        $this->queryStringParts = explode('/', $queryString);
        $this->postParams = $postParams;
    }

    /**
     * @return string
     */
    public function getQueryString(): string
    {
        return $this->queryString;
    }

    /**
     * @param $index
     * @return mixed|null
     */
    public function getQueryStringPart($index)
    {
        return $this->queryStringParts[$index] ?? null;
    }

    /**
     * @return array
     */
    public function getQueryStringParts(): array
    {
        return $this->queryStringParts;
    }

    /**
     * @param $name
     * @return null|string
     */
    public function getPostParam($name)
    {
        if (isset($this->postParams[$name])) {
            return htmlspecialchars($this->postParams[$name]);
        } else {
            return null;
        }
    }

    /**
     * @param $name
     * @return array|null
     */
    public function getPostArrayParam($name)
    {
        return $this->postParams[$name] ?? null;
    }

    /**
     * @return array
     */
    public function getPostParams(): array
    {
        return $this->postParams;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return bool
     */
    public function isGET()
    {
        return $this->method === self::METHOD_GET;
    }

    /**
     * @return bool
     */
    public function isPOST()
    {
        return $this->method === self::METHOD_POST;
    }
}