<?php


namespace Trial\Service;

use mysqli;
use mysqli_sql_exception;

class DBConnector
{
    const HOST = '127.0.0.1';
    const USER = 'root';
    const PASSWORD = '';
    const DB_NAME = 'trial';

    /** @var mysqli */
    protected $connection;

    public function __construct()
    {
        $this->connection = new mysqli(
            self::HOST, self::USER, self::PASSWORD, self::DB_NAME);
        if ($this->connection->connect_errno) {
            throw new mysqli_sql_exception('Failed to connect to MySQL: ' . $this->connection->connect_error);
        }
    }
}