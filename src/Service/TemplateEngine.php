<?php

namespace Trial\Service;


class TemplateEngine
{
    /** @var string */
    private $viewsDir;

    public function __construct()
    {
        $this->viewsDir = BASE_DIR . '/app/views/';
    }

    /**
     * @param $templateName string
     * @param $values array
     */
    public function render($templateName, array $values = null) {
        require_once $this->viewsDir . $templateName;
    }
}