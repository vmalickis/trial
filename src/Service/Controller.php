<?php


namespace Trial\Service;


class Controller
{
    /** @var TemplateEngine */
    private $templateEngine;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->templateEngine = new TemplateEngine();
    }

    /**
     * @param $templateName string
     * @param $values array
     */
    protected function render($templateName, array $values = null) {
        $this->templateEngine->render($templateName, $values);
    }

}