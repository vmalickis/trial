<?php


namespace Trial\Form;


use Trial\Product\Products\Book;
use Trial\Product\Products\DiscStorage;
use Trial\Product\Products\Furniture;
use Trial\Product\ProductTypes;
use Trial\Service\Http\Request;
use Trial\Product\Factory\DiscStorageFactory;
use Trial\Product\Factory\BookFactory;
use Trial\Product\Factory\FurnitureFactory;


class ProductForm implements Form
{
    /**
     * @param Request $request
     * @return null|Book|DiscStorage|Furniture
     */
    public function handle(Request $request)
    {
        $product = null;

        switch ($request->getPostParam('type')) {
            case ProductTypes::TYPE_WITH_SIZE:
                /** @var DiscStorage $product */
                $product = (new DiscStorageFactory())
                            ->produce($request->getPostParam('sku'),
                                      $request->getPostParam('name'),
                                      $request->getPostParam('price'),
                                      $request->getPostParam('type'),
                                      $request->getPostParam('size'))
                ;
                return $product;

            case ProductTypes::TYPE_WITH_WEIGHT:
                /** @var Book $product */
                $product = (new BookFactory())
                            ->produce($request->getPostParam('sku'),
                                      $request->getPostParam('name'),
                                      $request->getPostParam('price'),
                                      $request->getPostParam('type'),
                                      $request->getPostParam('weight'))
                ;
                return $product;

            case ProductTypes::TYPE_WITH_DIMENSIONS:
                /** @var Furniture $product */
                $product = (new FurnitureFactory())
                            ->produce($request->getPostParam('sku'),
                                      $request->getPostParam('name'),
                                      $request->getPostParam('price'),
                                      $request->getPostParam('type'),
                                      $request->getPostParam('height'),
                                      $request->getPostParam('width'),
                                      $request->getPostParam('length'))
                ;
                return $product;
        }
    }
}