<?php


namespace Trial\Form;


use Trial\Service\Http\Request;


interface Form
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function handle(Request $request);
}