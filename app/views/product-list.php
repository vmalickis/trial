<?php

use Trial\Product\AbstractProduct;
use Trial\Product\ProductTypes;
use Trial\Product\ProductWithSize;
use Trial\Product\ProductWithWeight;

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Product</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <form id="product_form" method="POST">
            <div class="row">
                <div class="col-md-10">
                    <h2>Product List</h2>
                </div>
                <div class="col-md-2">
                    <div class="form-group" style="margin-top: 19px">
                        <select id="action_select" class="form-control" style="width: auto">
                            <option value="delete">Mass Delete Action</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input class="btn btn-default"  type="submit" value="Apply">
                    </div>
                </div>
            </div>
            <hr>
            <?php foreach ($values['products'] as $product): ?>
                <div class="col-md-3 border border-secondary">
                    <div class="thumbnail">

                        <input name="product[]" type="checkbox" value="<?= $product->getId();?>"/>

                        <div class="caption" style="text-align: center">
                            <h4><?= $product->getSku() ;?></h4>
                            <p><?= $product->getName() ;?></p>
                            <p><?= $product->getViewLayerPrice() . ' ' . AbstractProduct::PRICE_CURRENCY ;?></p>
                            <?php switch ($product->getType()):
                                case ProductTypes::TYPE_WITH_SIZE: ?>
                                    <p>Size: <?= $product->getSize()
                                        . ' ' . ProductWithSize::SIZE_MEASUREMENT_UNIT ;?>
                                    </p>
                                    <?php break;?>

                                <?php case ProductTypes::TYPE_WITH_WEIGHT: ?>
                                    <p>Weight: <?= $product->getWeight()
                                        . ' ' . ProductWithWeight::WEIGHT_MEASUREMENT_UNIT ;?>
                                    </p>
                                    <?php break;?>

                                <?php case ProductTypes::TYPE_WITH_DIMENSIONS: ?>
                                    <p>Dimensions:<?= $product->getViewLayerDimensions() ;?></p>
                                    <?php break;?>
                                <?php endswitch ;?>
                        </div>
                    </div>
                </div>
            <?php endforeach ;?>
        </form>
    </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script>
    //Custom script
    $('#product_form').submit(function() {
        $(this).attr('action',$('#action_select').val());
        $(this).submit();
    });
    
</script>
</body>
</html>

