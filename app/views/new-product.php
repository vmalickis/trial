<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Product</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <form method="post">
                <div class="row">
                    <div class="col-md-10"><h2>Product Add</h2></div>
                    <div class="col-md-2">
                        <input style="margin-top: 19px" class="btn btn-default"  type="submit" value="Save">
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>SKU<input type="text" name="sku" class="form-control"></label>
                        </div>
                        <div class="form-group">
                            <label>Name<input type="text" name="name" class="form-control"></label>
                        </div>
                        <div class="form-group">
                            <label>Price<input type="text" name="price" class="form-control"></label>
                        </div>

                        <div class="form-group">
                            <label>Product Type<br>
                                <select id="type_switch" class="form-control" name="type">
                                    <option selected disabled>Select product type</option>
                                    <?php foreach ($values['productTypes'] as $productType) {
                                        echo '<option value="' . $productType->getId() . '">'
                                            . $productType->getType()
                                            . '</option>';
                                    }
                                    ?>
                                </select>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6">

                        <div id="size_attribute" class="form-group hidden">
                            <label>Size<input type="text" name="size" class="form-control"></label>
                        </div>
                        <div id="weight_attribute" class="form-group hidden">
                            <label>Weight<input type="text" name="weight" class="form-control"></label>
                        </div>
                        <div id="dimensions" class="hidden">
                            <div class="form-group">
                                <label>Height<input type="text" name="height" class="form-control"></label>
                            </div>
                            <div class="form-group">
                                <label>Width<input type="text" name="width" class="form-control"></label>
                            </div>
                            <div class="form-group">
                                <label>Length<input type="text" name="length" class="form-control"></label>
                            </div>
                        </div>

                    </div>
                </div>
            </form>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script>
            //Custom script
            function hideSpeacialAttributes() {
                $('#size_attribute').addClass('hidden');
                $('#weight_attribute').addClass('hidden');
                $('#dimensions').addClass('hidden');
            }

            $('#type_switch').change(function () {

               var optionValue = $('select option:selected').val();
               hideSpeacialAttributes();
               switch (optionValue) {
                   case '1':
                       $('#size_attribute').removeClass('hidden');
                       break;
                   case '2':
                       $('#weight_attribute').removeClass('hidden');
                       break;
                   case '3':
                       $('#dimensions').removeClass('hidden');
                       break;
               }
            });

        </script>
    </body>
</html>
