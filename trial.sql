--
-- Use MySQL to create database named: `trial`
--
CREATE DATABASE `trial`
  CHARACTER SET utf8mb4;
--

--
-- Table structure for table `product`
--
CREATE TABLE `product` (
  `id` int(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
  `sku` varchar(64) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` decimal(15,4) UNSIGNED NOT NULL,
  `type_id` INT UNSIGNED
);

--
-- Table structure for table `product_type`
--
CREATE TABLE `product_type` (
  `id` int(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
  `type` varchar(100) NOT NULL
);

--
-- Dumping data for table `product_type`
--
INSERT INTO `product_type` (`type`) VALUES
  ('disk'),
  ('book'),
  ('furniture');

--
-- Add relation many to one, between tables `product` and `product_type`
--
ALTER TABLE `product`
  ADD CONSTRAINT FK_PRODUCT_TYPE_ID
FOREIGN KEY (type_id) REFERENCES product_type (id);

--
-- Table structure for table `product_size`
--
CREATE TABLE `product_size` (
  `product_id` INT UNSIGNED,
  `size` FLOAT UNSIGNED NOT NULL
);

--
-- Add relation one to one, between tables `product` and `product_size`
--
ALTER TABLE `product_size`
  ADD CONSTRAINT FK_PRODUCT_WITH_SIZE
FOREIGN KEY (product_id) REFERENCES `product` (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

--
-- Table structure for table `product_weight`
--
CREATE TABLE `product_weight` (
  `product_id` INT UNSIGNED,
  `weight` FLOAT UNSIGNED NOT NULL
);

--
-- Add relation one to one, between tables `product` and `product_size`
--
ALTER TABLE `product_weight`
  ADD CONSTRAINT FK_PRODUCT_WITH_WEIGHT
FOREIGN KEY (product_id) REFERENCES `product` (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

--
-- Table structure for table `product_dimensions`
--
CREATE TABLE `product_dimensions` (
  `product_id` INT UNSIGNED,
  `height` FLOAT UNSIGNED NOT NULL,
  `width` FLOAT UNSIGNED NOT NULL,
  `length` FLOAT UNSIGNED NOT NULL
);

--
-- Add relation one to one, between tables `product` and `product_dimensions`
--
ALTER TABLE `product_dimensions`
  ADD CONSTRAINT FK_PRODUCT_WITH_DIMENSIONS
FOREIGN KEY (product_id) REFERENCES `product` (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE;