<?php

use Trial\ProductController;
use Trial\Service\Http\Request;

require __DIR__ . '/vendor/autoload.php';

define('BASE_DIR', __DIR__);

$controller = new ProductController();

if (isset($_GET['query'])) {
    $controller->process(new Request($_SERVER['REQUEST_METHOD'], $_GET['query'], $_POST));
} else {
    echo
    '<ul>
        <li><a href="product/list"/>Product List</li>
        <li><a href="product/new"/>Add New Product</li>
    </ul>';
}

